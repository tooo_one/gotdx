module gitee.com/quant1x/gotdx

go 1.21.5

require (
	gitee.com/quant1x/exchange v0.4.0
	gitee.com/quant1x/gox v1.20.2
	gitee.com/quant1x/num v0.1.6
	gitee.com/quant1x/pkg v0.2.6
	golang.org/x/text v0.14.0
)

require (
	github.com/dlclark/regexp2 v1.11.0 // indirect
	github.com/go-sourcemap/sourcemap v2.1.3+incompatible // indirect
	github.com/google/pprof v0.0.0-20240227163752-401108e1b7e7 // indirect
	golang.org/x/sys v0.17.0 // indirect
)
